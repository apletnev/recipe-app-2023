
export const getRecipeCards = (recipes, parentElement, createElement) => {
    
    for (let recipe of recipes){

        getRecipeCard(recipe, parentElement, createElement);

    }    
}

export const getRecipeCard = (recipe, parentElement, createElement) => {
 
    const cardContainer  = createElement("div");
    cardContainer.classList.add("card", "shadow");

    const cardImageContainer = createElement("div");
    cardImageContainer.classList.add("card-image-container");

    const imageElement = createElement("img");
    imageElement.classList.add("card-image");
    imageElement.setAttribute("src", recipe["image-url"]);
    imageElement.setAttribute("alt", recipe.TranslatedRecipeName);
    imageElement.setAttribute("data-id", recipe.ID);
    cardImageContainer.appendChild(imageElement);

    cardContainer.appendChild(cardImageContainer);

    const cardDetailsContainer = createElement("div");
    cardDetailsContainer.classList.add("recipe-details");

    const cardTitleElement = createElement("div");
    cardTitleElement.classList.add("title");
    cardTitleElement.innerText = recipe.TranslatedRecipeName;
    cardDetailsContainer.appendChild(cardTitleElement);

    const cardRatingAndTime = createElement("div");
    cardRatingAndTime.classList.add("ratings");

    const cardRatingContainer = createElement("div");

    const ratingValueElement = createElement("span");
    ratingValueElement.innerText = `Cuisine: ${recipe.Cuisine}`;
    cardRatingContainer.appendChild(ratingValueElement);

    cardRatingAndTime.appendChild(cardRatingContainer);

    const timeElement = createElement("div");
    timeElement.classList.add("star-rating");

    const ratingIconElement = createElement("span");
    ratingIconElement.classList.add("time", "material-icons-outlined");
    ratingIconElement.innerText = "timer";
    timeElement.appendChild(ratingIconElement);
    const duration = createElement("span");
    duration.innerText = `${recipe.TotalTimeInMins} min`;
    timeElement.appendChild(duration);

    cardRatingAndTime.appendChild(timeElement);

    cardDetailsContainer.appendChild(cardRatingAndTime);

    cardContainer.appendChild(cardDetailsContainer);

    parentElement.appendChild(cardContainer);

}

export const getSingleRecipeCard = (recipe, parentElement, createElement) => {
 
    const cardContainer  = createElement("div");
    cardContainer.classList.add("single-card", "shadow");

    const cardImageContainer = createElement("div");
    cardImageContainer.classList.add("card-image-container");

    const imageElement = createElement("img");
    imageElement.classList.add("card-image");
    imageElement.setAttribute("src", recipe["image-url"]);
    imageElement.setAttribute("alt", recipe.TranslatedRecipeName);
    imageElement.setAttribute("data-id", recipe.ID);
    cardImageContainer.appendChild(imageElement);

    cardContainer.appendChild(cardImageContainer);

    const cardDetailsContainer = createElement("div");
    cardDetailsContainer.classList.add("recipe-details");

    const cardTitleElement = createElement("div");
    cardTitleElement.classList.add("title");
    cardTitleElement.innerText = recipe.TranslatedRecipeName;
    cardDetailsContainer.appendChild(cardTitleElement);

    //++ inserting ingredients and instructions
    const ingredientsElement = createElement("div");
    ingredientsElement.innerText = `Ingredients: ${recipe.TranslatedIngredients}`;
    cardDetailsContainer.appendChild(ingredientsElement);

    const instructionsElement = createElement("div");
    instructionsElement.innerText = `Instructions: ${recipe.TranslatedInstructions}`;
    cardDetailsContainer.appendChild(instructionsElement);    
    //-- inserting ingredients and instructions

    const cardRatingAndTime = createElement("div");
    cardRatingAndTime.classList.add("ratings");

    const cardRatingContainer = createElement("div");
    const ratingValueElement = createElement("span");
    ratingValueElement.innerText = `Cuisine: ${recipe.Cuisine}`;
    cardRatingContainer.appendChild(ratingValueElement);

    cardRatingAndTime.appendChild(cardRatingContainer);

    const timeElement = createElement("div");
    timeElement.classList.add("star-rating");

    const ratingIconElement = createElement("span");
    ratingIconElement.classList.add("time", "material-icons-outlined");
    ratingIconElement.innerText = "timer";
    timeElement.appendChild(ratingIconElement);
    const duration = createElement("span");
    duration.innerText = `${recipe.TotalTimeInMins} min`;
    timeElement.appendChild(duration);

    cardRatingAndTime.appendChild(timeElement);



    cardDetailsContainer.appendChild(cardRatingAndTime);

    cardContainer.appendChild(cardDetailsContainer);

    parentElement.appendChild(cardContainer);

}
