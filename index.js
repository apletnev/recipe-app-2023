
import { getRecipeCards } from "./getRecipeCard.js";
import { getCuisineCard } from "./getCuisineCard.js";
import { getData } from "./getData.js";

const cardParentContainer    = document.querySelector("main");
const cuisineParentContainer = document.querySelector(".cuisine-filter");
const searchEl = document.querySelector(".input");

const recipeURL  = "https://recipeapi.prakashsakari.repl.co/api/recipes";
const cuisineURL = "https://recipeapi.prakashsakari.repl.co/api/recipes/cuisines";

let searchValue = "";
let filteredArrayOfRecipes = [];
let arrOfSelectedCuisine   = [];

const createElement = (element) => document.createElement(element);

const recipes  = await getData(recipeURL);
const cuisines = await getData(cuisineURL);

getRecipeCards(recipes, cardParentContainer, createElement);
getCuisineCard(cuisines, cuisineParentContainer, createElement);

const getFilteredData = () => {
    
    //search box input
    filteredArrayOfRecipes = 
         searchValue?.length > 0 
         ? recipes.filter((recipe) => recipe.TranslatedRecipeName.toLowerCase().includes(searchValue))
         : recipes;
    
    //cuisine checkbox
    if (arrOfSelectedCuisine?.length > 0){
        filteredArrayOfRecipes = searchValue?.length < 1 ? recipes: filteredArrayOfRecipes;
        filteredArrayOfRecipes = filteredArrayOfRecipes.filter((recipe) => arrOfSelectedCuisine.includes(recipe.Cuisine)); 
    }

    return filteredArrayOfRecipes;
}

const searchInputHandler = (ev) => {
    searchValue = ev.target.value.toLowerCase();
    const filteredData = getFilteredData();
    cardParentContainer.innerHTML = "";
    getRecipeCards(filteredData, cardParentContainer, createElement);
}

searchEl.addEventListener("keyup", searchInputHandler);

const handleCuisineClick = (ev) => {
    const id = ev.target.dataset.id;
    const isSelected = ev.target.checked;
    const selectedCuisine = cuisines.reduce(
        (acc, cur) => (cur.ID === acc ? cur.Cuisine : acc), id
    );
    arrOfSelectedCuisine = isSelected ? [...arrOfSelectedCuisine, selectedCuisine] : arrOfSelectedCuisine.filter((cuisine) => cuisine !== selectedCuisine);
    const filteredArrOfCuisine = getFilteredData();
    cardParentContainer.innerHTML = "";
    getRecipeCards(filteredArrOfCuisine, cardParentContainer, createElement);
}

cuisineParentContainer.addEventListener("click", handleCuisineClick);

cardParentContainer.addEventListener("click", (ev) => {
    //console.log(ev.target.dataset.id);
    const cardId = ev.target.dataset.id;
    if (cardId){
        localStorage.clear();
        localStorage.setItem("recipe-id", cardId);
        location.href = "single-recipe.html";
    }
})