import { getData } from "./getData.js";
import { getSingleRecipeCard } from "./getRecipeCard.js";

const createElement = (element) => document.createElement(element);

const cardId = localStorage.getItem("recipe-id");

const SINGLERECIPEURL = `https://recipeapi.prakashsakari.repl.co/api/recipes/${cardId}`;

const singleRecipe = await getData(SINGLERECIPEURL);

const cardParentContainer = document.querySelector("main");

console.log(singleRecipe[0]);

getSingleRecipeCard(singleRecipe[0], cardParentContainer, createElement);