
export const getData = async(URL) => {
    try {
        const {data} = await axios.get(URL);
        return data;
    } catch(err) {
        console.log(err);
    }
}